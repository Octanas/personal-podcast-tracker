# Personal Podcast Tracker

**Personal Podcast Tracker** is a simple app for tracking podast. A podcast can be loaded with its RSS feed and the app will notify new episode releases and let the user track the episodes they've listened to.

**NOTE:** this app does not play podcast episodes (atleast for now)